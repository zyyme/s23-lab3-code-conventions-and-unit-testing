# Lab3 -- Unit testing

[![pipeline status](https://gitlab.com/zyyme/s23-lab3-code-conventions-and-unit-testing/badges/master/pipeline.svg)](https://gitlab.com/zyyme/s23-lab3-code-conventions-and-unit-testing/-/commits/master)

## Introduction

Actually, we have started a bit early with linters because one of the most important things that you should have in your projects is codestyle. One of the linter's goals is to check if the codestyle corresponds to what it is supposed to be. Before the start of development this is the first thing you should to discuss, so you will have readable and standardized code through all of your repository. Another important thing that we are going to do today is testing, one the whales on which the whole QA process is standing in the modern industry.
***Let's roll!***

## Unit testing

Well, maybe in the 1990s it wasn't necessary to write unit tests, but today every company is doing it with their code. They may not cover 100% of your code, but it is important to have unit testing to see if some crucial part of your functionality was broken. We will talk about different ways of testing later on, for now you will need just to check if functions are working.

**Patterns you should use**:
- Atomicity, Independency, Isolation
- Use mocks and stubs
- Use already developed libs, don't reinvent the wheel
- Write mocks for your modules
- Helpful environment for unit-tests
- Write testable code

**Antipatterns you shouldn't use**:
- Liar
- The Secret Catcher
- Slowpoke
- The Mockery
- Cuckoo (aka Stranger)
- Branching and cycles in tests
- You can read about them [***Here***](https://www.yegor256.com/2018/12/11/unit-testing-anti-patterns.html)

Stubs and mocks are both dummy objects for testing. While stubs only implement a pre-programmed response, mocks also pre-program specific expectations.

### Mocks

- Setup object - define the mock, what object you are mocking and how (similar to stubs)
- Setup expectations - define what you expect will happen to this mock internally
- Exercise - run the functionality you want to test
- Verify mock - verify that the mock expectations are met. In some JS libraries this happens automatically without additional call, the mock expectations are verifying themselves and will throw if needed. (Used mostly when async testing).
- Verify - verify any additional expectations for results on the mock
- Teardown - if needed, clean up.

Example:
```js
it("when purchase paid successfully user should receive a mail", function() {
  // Setup objects
  let userMock = sinon.mock({
    paymentMethod: () => {},
    sendSuccessMail: () => {}
  })

  // Setup expectations
  userMock.expect(paymentMethod).returns("success")
  userMock.expect(sendSuccessMail).once()

  // Exercise
  purchaseItemsFromCart([], user)

  // Verify mocks
  userMock.verify()
})
```

### Stubs

- Setup - define the stub itself, what object in the program you are stubbing and how
- Exercise - run the functionality you want to test
- Verify - check the stub for values that ran through it, that they fit expectations
- Teardown - if needed, clean up. e.g Time stubs are usually global, you need to yield control back

Example:
```js
const purchaseItemsFromCart = (cartItems, user) => {
  let payStatus = user.paymentMethod(cartItems)
  if (payStatus === "success") {
    user.sendSuccessMail()
  } else {
    user.redirect("payment_error_page")
  }
}

it("when purchase paid successfully user should receive a mail", function() {
  // Setup
  let paymentStub = sinon.stub().returns("success")
  let mailStub = sinon.stub()
  let user = {
    paymentMethod: paymentStub,
    sendSuccessMail: mailStub
  }

  // Exercise
  purchaseItemsFromCart([], user)

  // Verify
  assert(mailStub.called)
})
```

## Lab

This lab is going to be small compared to the previous ones, and there will be almost nothing interesting in here, let's just write few tests in lab, and the rest of them will be your homework(as well as integration of unit-testing with CI).
1. Create your fork of the `
Lab3 - Code conventions and unit testing
` repo and clone it.
2. We have few functions that I would like to test in `src/main/java/com/hw/db/controllers/ForumController.java`, which are few create functions, we are going to use mock and stub to implement two unit tests for them:

- Function to be tested:
```java
public ResponseEntity create(@RequestBody Forum forum) {
    User creator = new User();
    try {
        creator = UserDAO.Search(forum.getUser());
    } catch(DataAccessException Except) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Forum owner not found."));
    }

    forum.setUser(creator.getNickname());
    try {
        ForumDAO.CreateForum(forum);
    } catch (DuplicateKeyException Except) {
        Forum exForum = Info(forum.getSlug());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(exForum);
    } catch (DataAccessException Except) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new Message("Something on the server."));
    }
    return ResponseEntity.status(HttpStatus.CREATED).body(forum);
}
```

Now let's write a test:
```java
class ForumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        loggedIn = new User("some","some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @Test
    @DisplayName("Correct forum creation test")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                ForumController controller = new ForumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Result for succeeding forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
}
```

## Homework

As a homework you will need to test each of the functions in `src/main/java/com/hw/db/controllers/threadController.java` using both mocks and stubs. After that you should add your tests to the pipeline. Tests that you write should pass linter checks.
**Lab is counted as done, if pipelines are passing and tests are developed**
