package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ThreadControllerTests {
    private ThreadController threadController;
    private Thread thread;
    private Post post;
    private User user;

    @BeforeEach
    public void init() {
        threadController = new ThreadController();
        Timestamp timestamp = new Timestamp(0);
        thread = new Thread("test_author", timestamp, "test_forum", "test_message", "test_slug", "test_title", 0);
        thread.setId(0);
        post = new Post("test_author", timestamp, "test_forum", "test_message", 0, 0, false);
        user = new User("test_author", "test_email", "test_fullname", "test_about");
    }

    @Test
    @DisplayName("Test for CheckIdOrSlug method")
    public void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug")).thenReturn(thread);

            Assertions.assertEquals(thread, threadController.CheckIdOrSlug("0"));
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug("test_slug"));
        }
    }

    @Test
    @DisplayName("Test for createPost method")
    public void testCreatePost() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug")).thenReturn(thread);
                mockedUserDAO.when(() -> UserDAO.Info("test_author")).thenReturn(user);

                ResponseEntity responseEntity = threadController.createPost("0", Collections.singletonList(post));

                Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
                Assertions.assertEquals(Collections.singletonList(post), responseEntity.getBody());
            }
        }
    }

    @Test
    @DisplayName("Test for Posts method")
    public void testPosts() {
        List<Post> posts = Collections.singletonList(post);
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug")).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getPosts(0, 1, 0, "flat", false)).thenReturn(posts);
            
            ResponseEntity responseEntity = threadController.Posts("test_slug", 1, 0, "flat", false);

            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(posts, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test for change method")
    public void testChange() {
        Thread thread1 = new Thread("test_author", new Timestamp(0), "test_forum", "test_message", "test_slug1", "test_title", 0);
        thread1.setId(666);
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug")).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(666)).thenReturn(thread1);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug1")).thenReturn(thread1);

            ResponseEntity responseEntity = threadController.change("test_slug1", thread1);

            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread1, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test for info method")
    public void testInfo() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug")).thenReturn(thread);

            ResponseEntity responseEntity = threadController.info("test_slug");

            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test for createVote method")
    public void testCreateVote() {
        Vote vote = new Vote("test_author", 1);
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("test_slug")).thenReturn(thread);
                mockedUserDAO.when(() -> UserDAO.Info("test_author")).thenReturn(user);

                ResponseEntity responseEntity = threadController.createVote("test_slug", vote);

                Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                Assertions.assertEquals(thread, responseEntity.getBody());
            }
        }
    }
}